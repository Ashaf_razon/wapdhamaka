package com.avis.avistechbd.wapdhamaka.wap_dialog;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.avis.avistechbd.wapdhamaka.R;
import com.avis.avistechbd.wapdhamaka.activities.DhamakaOpenPrivacy;

public class CustomDialogTwo extends Dialog implements View.OnClickListener {

    public Activity activity;
    public Dialog d;
    public Button yes;
    TextView txtDia;
    int flagNo;

    public CustomDialogTwo(Activity activity, int flagNo) {
        super(activity);
        this.activity = activity;
        this.flagNo = flagNo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_two);
        yes = (Button) findViewById(R.id.btn_yes);
        yes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        dismiss();
        Intent getPrivacyP = new Intent(activity,DhamakaOpenPrivacy.class);
        activity.startActivity(getPrivacyP);
        activity.finish();
    }
}
