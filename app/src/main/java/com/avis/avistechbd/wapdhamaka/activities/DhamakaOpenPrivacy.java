package com.avis.avistechbd.wapdhamaka.activities;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.avis.avistechbd.wapdhamaka.R;
import com.avis.avistechbd.wapdhamaka.wap_dialog.mDialogClass;

public class DhamakaOpenPrivacy extends AppCompatActivity {
    private Handler handler;
    private Context context;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dhamaka_open_privacy);
        getSupportActionBar().hide();
        handler = new Handler(Looper.getMainLooper());
        context = DhamakaOpenPrivacy.this;
        pd = new ProgressDialog(context);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
    }

    @Override
    public void onBackPressed() {
        if(isNetworkAvailable()){
            Intent goMainWeb = new Intent(context,MyDhamakaWap.class);
            startActivity(goMainWeb);
            finish();
        }else{
            try{
         ;       customDialogCall();
            }catch (Exception e){
                super.onBackPressed();
            }
        }
    }

    private void customDialogCall(){
        mDialogClass myDialog = new mDialogClass((Activity) context,R.string.dialog_title,1);
        try{
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (Exception e){
            e.printStackTrace();
        }
        myDialog.setCancelable(false);
        myDialog.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //finish();
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}
