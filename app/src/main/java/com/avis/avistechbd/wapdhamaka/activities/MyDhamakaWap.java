package com.avis.avistechbd.wapdhamaka.activities;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.avis.avistechbd.wapdhamaka.R;
import com.avis.avistechbd.wapdhamaka.wap_dialog.CustomDialogTwo;
import com.avis.avistechbd.wapdhamaka.wap_dialog.mDialogClass;

public class MyDhamakaWap extends AppCompatActivity {
    private WebView myWebView;
    private Context context;
    private Handler handler;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_dhamaka_wap);
        getSupportActionBar().hide();
        context = MyDhamakaWap.this;
        handler = new Handler(Looper.getMainLooper());
        context = MyDhamakaWap.this;
        myWebView = findViewById(R.id.webView);

        pd = new ProgressDialog(context);
        pd.setTitle("Please wait!");
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        myViewThread();
    }

    @Override
    public void onBackPressed() {
        if(myWebView.canGoBack()) {
            myWebView.goBack();
        } else {
            try{
                customDialogCall();
            }catch (Exception e){
                super.onBackPressed();
            }
        }
    }

    private void customDialogCall(){
        mDialogClass myDialog = new mDialogClass((Activity) context,R.string.dialog_title,1);
        try{
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (Exception e){
            e.printStackTrace();
        }
        myDialog.setCancelable(false);
        myDialog.show();
    }

    private void myDialogTwo(){
        CustomDialogTwo myDialog = new CustomDialogTwo((Activity) context,1);
        try{
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (Exception e){
            e.printStackTrace();
        }
        myDialog.setCancelable(false);
        myDialog.show();
    }

    private void myViewThread() {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            if(isNetworkAvailable()){
                                callWebWap();
                            }else{
                                pd.dismiss();
                                myDialogTwo();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //finish();
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private void callWebWap() {
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.loadUrl("http://dhamaka.avistechbd.com/Home.aspx");
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                if (pd.isShowing()) {
                    pd.dismiss();
                }
            }
        });
    }
}
