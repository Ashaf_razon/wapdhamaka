package com.avis.avistechbd.wapdhamaka.activities;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class BestSplashTheme extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        Intent goWapSlider = new Intent(BestSplashTheme.this,SplashDhamaka.class);
        startActivity(goWapSlider);
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
