package com.avis.avistechbd.wapdhamaka.activities;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.avis.avistechbd.wapdhamaka.R;
import pl.droidsonroids.gif.GifImageView;

public class SplashDhamaka extends AppCompatActivity {
    private static long SPLASH_TIMER = 3000;
    private Context context;
    private Handler handler;
    private GifImageView myGifImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_splash_dhamaka);
        myGifImage = findViewById(R.id.gifImageViewControl);
        handler = new Handler(Looper.getMainLooper());
        context = SplashDhamaka.this;
        threadSplash();
    }

    private void threadSplash() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try{
                    Intent goWap = new Intent(context,MyDhamakaWap.class);
                    startActivity(goWap);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, SPLASH_TIMER);
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
