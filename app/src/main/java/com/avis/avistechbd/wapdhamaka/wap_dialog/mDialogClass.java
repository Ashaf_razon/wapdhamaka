package com.avis.avistechbd.wapdhamaka.wap_dialog;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.avis.avistechbd.wapdhamaka.R;

public class mDialogClass extends Dialog implements View.OnClickListener {
    public Activity activity;
    public Dialog d;
    public Button yes, no;
    TextView txtDia;
    int dialog_title;
    int flagNo;

    public mDialogClass(Activity activity, int dialog_title, int flagNo) {
        super(activity);
        this.activity = activity;
        this.dialog_title = dialog_title;
        this.flagNo = flagNo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        txtDia = (TextView) findViewById(R.id.txt_dia);
        txtDia.setText(dialog_title);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                activity.finish();
                break;
            case R.id.btn_no:
                dismiss();
                if(flagNo == 3){
                    activity.finish();
                }
                break;
            default:
                break;
        }
        dismiss();
    }
}
